package ds.core.service;

import java.util.List;

import ds.core.models.User;

public interface UserService {
	List<User> findAllUser();
	User findByUserName(String userName);
	User save(User user);
}
