package ds.core.service;

import java.util.List;

import ds.core.models.Photo;
import ds.core.models.User;

public interface PhotoService {

	Photo save(Photo photo);
	List<Photo> findAll();
	List<Photo> findByUser(User user);
	Photo findByPhotoId(Long photoId);
	
}
