package ds.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ds.core.dao.PhotoDao;
import ds.core.models.Photo;
import ds.core.models.User;
import ds.core.service.PhotoService;

@Service
public class PhotoServiceImpl implements PhotoService {

	@Autowired
	private PhotoDao photoDao; 
	
	
	@Override
	public Photo save(Photo photo) {
		// TODO Auto-generated method stub
		return photoDao.save(photo);
	}

	@Override
	public List<Photo> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Photo> findByUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Photo findByPhotoId(Long photoId) {
		// TODO Auto-generated method stub
		return null;
	}

}
