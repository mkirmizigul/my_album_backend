package ds.core.controller;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import ds.core.models.Photo;
import ds.core.models.User;
import ds.core.service.PhotoService;

@RestController
@RequestMapping()
public class PhotoResource {
	
	private String imageName;
	
	@Autowired
	private PhotoService photoService;
	
	@RequestMapping(value="photo/upload",method=RequestMethod.POST)
	public String upload(HttpServletResponse resp,HttpServletRequest req){
		MultipartHttpServletRequest multipart=(MultipartHttpServletRequest) req;
		Iterator<String> it=multipart.getFileNames();
		
		MultipartFile multiPartFile=multipart.getFile(it.next());
		
		String fileName=multiPartFile.getOriginalFilename();
		
		imageName=fileName;
		
		String path=new File("target/classes/static/images").getAbsolutePath()+"\\"+fileName;
		
		try {
			multiPartFile.transferTo(new File(path));
			System.out.println(path);
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return "Upload Success!";
	}
	
	@RequestMapping(value="photo/add",method=RequestMethod.POST)
	public Photo addPhoto(@RequestBody Photo photo){
		photo.setImageName(imageName);
		return photoService.save(photo);
	}
	
	@RequestMapping(value="photo/user",method=RequestMethod.POST)
	public List<Photo> getPhotoByUser(@RequestBody User user){
		return photoService.findByUser(user);
	}
	
	@RequestMapping(value="photo/user",method=RequestMethod.POST)
	public void updatePhoto(@RequestBody Photo photo){
		Photo currentPhoto=photoService.findByPhotoId(photo.getPhotoId());
		currentPhoto.setLikes(photo.getLikes());
		photoService.save(currentPhoto);
	}
	
}
