package ds.core.controller;

import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;

import org.hibernate.boot.model.source.spi.SingularAttributeNature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ds.core.models.User;
import ds.core.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(@RequestBody Map<String,String> json) throws ServletException{
		if(json.get("username")==null||json.get("password")==null){
			throw new ServletException("Please fill in username and password");
		}
		
		String username=json.get("username");
		String password=json.get("password");
		
		User user=userService.findByUserName(username);
		 
		if (user==null) {
			throw new ServletException("Username not found");
		}
		
		return Jwts.builder().setSubject(username).claim("roles", "user").setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, "secretkey").compact();
	}
	
	@RequestMapping(value="/register",method=RequestMethod.POST)
	public User registerService(@RequestBody User user){
		return userService.save(user);
	}
}
